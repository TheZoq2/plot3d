# Plot3d

![screenshot](screenshot.png "screenshot")

A small rust program to plot 4d data as a surface plot that changes over time.

## Usage:

Create two files containing the coordinates of the 'x' and 'y' axis. For example
values ranging between -5 and 5 with a step of 0.5 should look like this

```
-5,
-4.5
-4
...
5
```

The Z values of the function vary over time, and each file line in the file containing
z-data should contain the z values at one time step. The z-values should be stored
as a 1 dimensional array separated by commas.

For example: 

```
f(t0, x0, y0), f(t0, x1, y0) ... f(t0, xn, y0) ... f(t0, x0, yn) ... f(t0, xn, yn)
f(t1, x0, y0), f(t1, x1, y0) ... f(t1, xn, y0) ... f(t1, x0, yn) ... f(t1, xn, yn)
...
f(tn, x0, y0), f(tn, x1, y0) ... f(tn, xn, y0) ... f(tn, x0, yn) ... f(tn, xn, yn)
```

You can then call the program using
`plot3d <x_axis_file> <y_axis_file> <z_axis_file>`

The speed of the animation can be specified with the `-s` flag which controls
how many time steps are rendered per second.



