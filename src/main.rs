use nalgebra as na;

mod error;

use std::cell::RefCell;
use std::rc::Rc;


use std::path::{Path, PathBuf};
use std::fs::File;
use std::io::prelude::*;
use std::thread;
use std::sync::mpsc;


use na::{
    Isometry3,
    Matrix3,
    Matrix4,
    Point2,
    Point3,
    Vector3,
    Translation3,
};
use kiss3d::camera::Camera;
use kiss3d::context::Context;
use kiss3d::light::Light;
use kiss3d::resource::{Effect, Material, Mesh, ShaderAttribute, ShaderUniform};
use kiss3d::scene::ObjectData;
use kiss3d::window::Window;
use kiss3d::text::Font;
use kiss3d::event::{Action, WindowEvent, Key};
use clap::{App, Arg};

use log::{warn};

enum Command {
    SetTime(usize)
}

fn read_1d_array(filename: &Path) -> std::io::Result<Vec<f32>> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    Ok(contents.split("\n")
        .filter(|x| !x.is_empty())
        .map(|x| x.trim())
        .map(|x| x.parse::<f32>().expect("Failed to parse float"))
        .collect()
    )
}

fn read_xy_time_coords(filename: &Path) -> Result<Vec<Vec<Point2<f32>>>, failure::Error> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let point_parser = |point: &str| -> Result<Point2<f32>, failure::Error> {
        let coord_strings = point.split(",").collect::<Vec<&str>>();
        if coord_strings.len() != 2 {
            let message = format!(
                    "Incorrect number of coordinates in coord string: {} in {}",
                    point,
                    filename.to_string_lossy()
                );
            Err(error::Error::from(message))?;
        }
        let x_coord = coord_strings[0]
            .trim()
            .parse::<f32>()?;
        let y_coord = coord_strings[1]
            .trim()
            .parse::<f32>()?;
        Ok(Point2::new(x_coord, y_coord))
    };

    contents.split("\n")
        .map(|x| {
            if x.is_empty() {
                Ok(vec!())
            }
            else {
                x.split(":")
                    .map(point_parser)
                    .collect::<Result<_, failure::Error>>()
            }
        })
        .collect::<Result<_, failure::Error>>()
}

struct ScalingParameters {
    pub min: f32,
    pub max: f32,
    pub scale: f32,
}

impl ScalingParameters {
    pub fn apply(&self, target: f32) -> f32 {
        (target - self.min) * self.scale
    }
}

fn scaling_parameters(points: &[f32]) -> ScalingParameters {
    let min = points.iter().cloned().fold(0./0., f32::min);
    let max = points.iter().cloned().fold(-0./0., f32::max);

    ScalingParameters {
        max,
        min,
        scale: 1. / (max - min)
    }
}

#[derive(Clone, Debug)]
enum MaybePoint {
    Valid(Point3<f32>),
    Invalid(Point2<f32>),
}

const INVALID_POINT_Y: f32 = -0.01;
impl MaybePoint {
    pub fn into_option(&self) -> Option<Point3<f32>> {
        match self {
            MaybePoint::Valid(point) => Some(point.clone()),
            MaybePoint::Invalid(_) => None
        }
    }

    pub fn into_valid(self) -> Point3<f32> {
        match self {
            MaybePoint::Valid(point) => point,
            MaybePoint::Invalid(point) => Point3::new(point.x, INVALID_POINT_Y, point.y)
        }
    }

    pub fn make_valid(self) -> Self {
        match self {
            point @ MaybePoint::Valid(_) => point,
            MaybePoint::Invalid(point) => MaybePoint::Valid(Point3::new(point.x, INVALID_POINT_Y, point.y))
        }
    }

    fn map<F>(self, f: F) -> MaybePoint
        where F: Fn(Point3<f32>) -> Point3<f32>
    {
        match self {
            MaybePoint::Valid(point) => MaybePoint::Valid(f(point)),
            invalid => invalid
        }
    }
}


fn command_line_input(sender: mpsc::Sender<Command>) {
    let stdin = std::io::stdin();
    loop {
        let mut content = String::new();
        stdin.read_line(&mut content).unwrap();

        let words = content.split(" ").collect::<Vec<_>>();

        if words.len() > 1 {
            if words[0] == "jump" {
            }
        }
    }
}

fn main() {
    simple_logger::init().unwrap();

    // Read command line input
    let matches = App::new("plot3d")
        .version("0.1.0")
        .arg(Arg::with_name("X_POINTS")
             .help("File containing X-axis coordinates")
             .required(true)
             .index(1))
        .arg(Arg::with_name("Y_POINTS")
             .help("File containing Y-axis coordinates")
             .required(true)
             .index(2))
        .arg(Arg::with_name("Z_POINTS")
             .help("File containing Z-axis coordinates")
             .required(true)
             .index(3))
        .arg(Arg::with_name("VERTICAL_LINES")
            .help(
                r#"File containing (x,y) coordinates where a vertical line should be drawn. Each
                line in the file corresponds to one time step. Points should be separated by
                :, coordiantes by comma. Each time step can have as many or as few points as
                desired"#
            )
            .takes_value(true)
            .short("l"))
        .arg(Arg::with_name("fps")
            .short("s")
            .takes_value(true)
            .help("Amount of time steps to render per second"))
        .get_matches();

    let x_points_file = matches.value_of("X_POINTS").unwrap();
    let x_points = read_1d_array(Path::new(x_points_file))
        .expect("Failed to read x points");

    let y_points_file = matches.value_of("Y_POINTS").unwrap();
    let y_points = read_1d_array(Path::new(y_points_file))
        .expect("Failed to read y points");

    let fps = matches.value_of("fps").unwrap_or("1").parse::<usize>()
        .expect("fps must be an integer");

    let vertical_lines_file = matches.value_of("VERTICAL_LINES");
    let vertical_lines = vertical_lines_file
        .map(|filename| read_xy_time_coords(&PathBuf::from(filename)).expect("Failed to read"));

    let x_scaling = scaling_parameters(&x_points);
    let y_scaling = scaling_parameters(&y_points);



    // let stdin = io::stdin();
    let time_steps = {
        let mut time_steps = vec!();
        let filename = matches.value_of("Z_POINTS").unwrap();
        let mut file = File::open(filename).expect("Failed to open z point file");
        let mut content = String::new();
        file.read_to_string(&mut content).expect("Failed to read z points");

        for line in content.split("\n").filter(|x| !x.is_empty()) {
            let values = line.split(",")
                .filter(|val| !val.is_empty())
                .map(|val| if val == "None" {None} else {Some(val)})
                .map(|val| val.map(|val| val.parse::<f32>().unwrap()))
                .collect::<Vec<_>>();

            let mut points = vec![];
            for y in 0..y_points.len() {
                // points.push(vec![]);
                for x in 0..x_points.len() {
                    // points[x].push(x * y_size + y);
                    let value = values[x * y_points.len() + y];
                    // let value = if value == 10000. {0.} else {value};
                    points.push(match value {
                        Some(value) => {
                            MaybePoint::Valid(
                                Point3::new(
                                    x_scaling.apply(x_points[x]),
                                    value,
                                    y_scaling.apply(y_points[y])
                                )
                            )
                        }
                        None =>
                            MaybePoint::Invalid(
                                Point2::new(
                                    x_scaling.apply(x_points[x]),
                                    y_scaling.apply(y_points[y])
                                )
                            )

                    })
                }
            }
            time_steps.push(points)
        }
        time_steps
    };

    let mut window = Window::new("Kiss3d: cube");


    // plane.set_color(1.0, 0.0, 0.0);

    window.set_light(Light::StickToCamera);

    // let rot = UnitQuaternion::from_axis_angle(&Vector3::x_axis(), 3.14/2.);

    // plane.prepend_to_local_rotation(&rot);
    let font = Font::default();

    let material = Rc::new(RefCell::new(
        Box::new(YColorMaterial::new()) as Box<dyn Material + 'static>
    ));
    let wireframe_material = Rc::new(RefCell::new(
        Box::new(BlackLineMaterial::new()) as Box<dyn Material + 'static>
    ));

    let (command_tx, command_rx) = mpsc::channel();
    // Read command line input in another thread
    std::thread::spawn(|| {command_line_input(command_tx)});

    let mut i = 0;
    let mut paused = false;
    let mut draw_relative_values = true;
    let mut enable_capping = false;
    let time_steps_per_iteration = 60/fps;

    let y_size = 0.5;
    let cap_value = 50.;

    'main: loop {
        if !paused {
            i = i + 1;
        }
        let current_timestep = (i/time_steps_per_iteration) % time_steps.len();

        draw_state_info(
            &mut window,
            &font,
            current_timestep,
            draw_relative_values,
            enable_capping
        );

        if let Some(ref lines) = vertical_lines {
            let this_frame = lines.get(current_timestep);
            if let Some(to_draw) = this_frame {
                for coords in to_draw {
                    let x = x_scaling.apply(coords.x);
                    let z = y_scaling.apply(coords.y);
                    let start = Point3::new(x, 0., z);
                    let end = Point3::new(x, y_size, z);
                    window.draw_line(&start, &end, &Point3::new(1., 1.0, 1.0));
                }
            }
            else {
                warn!("Line file length is shorter than surface file length");
            }
        };


        let points = {
            let base = time_steps[current_timestep].clone();

            if !draw_relative_values {
                base.into_iter()
                    .map(MaybePoint::make_valid)
                    .collect::<Vec<_>>()
            }
            else {
                let min_valid_value = base.iter()
                    .filter_map(MaybePoint::into_option)
                    .map(|p| p.y)
                    .fold(0./0., f32::min);

                base.into_iter()
                    // Scale values
                    .map(|maybe_p| maybe_p.map(|p| Point3::new(
                        p.x,
                        p.y-min_valid_value,
                        p.z
                    )))
                    // Remove overly large values
                    .map(|maybe_p| maybe_p.map(|p| {
                        let y = if p.y > cap_value && enable_capping {
                            cap_value
                        }
                        else {
                            p.y
                        };
                        Point3::new( p.x, y, p.z)
                    }))
                    .collect::<Vec<_>>()
            }
        };

        let y_coords = &points.iter()
            .filter_map(MaybePoint::into_option)
            .map(|p| p.y)
            .collect::<Vec<_>>();
        let scaling = scaling_parameters(y_coords);

        window.draw_text(
            &format!("Z-max: {}", scaling.max),
            &Point2::new(0.0, 200.0),
            60.0,
            &font,
            &Point3::new(1.0, 1.0, 0.0),
        );

        let mut plane = window.add_quad_with_vertices(
            &points.clone().into_iter()
                .map(MaybePoint::into_valid)
                .map(|mut p| {p.y = scaling.apply(p.y); p})
                .collect::<Vec<_>>(),
            x_points.len(),
            y_points.len()
        );
        plane.set_local_scale(1., y_size, 1.);
        // plane.set_color(1., 0., 0.);
        plane.set_material(material.clone());
        plane.enable_backface_culling(false);

        let mut wireframe_plane = window.add_quad_with_vertices(
            &points.into_iter()
                .map(MaybePoint::into_valid)
                .map(|mut p| {p.y = scaling.apply(p.y); p})
                .collect::<Vec<_>>(),
            x_points.len(),
            y_points.len()
        );
        wireframe_plane.set_local_scale(1., y_size, 1.);
        wireframe_plane.set_local_translation(Translation3::new(0., 0.001, 0.));
        // plane.set_color(1., 0., 0.);
        wireframe_plane.set_material(wireframe_material.clone());
        wireframe_plane.enable_backface_culling(false);

        draw_grid(&mut window);
        draw_axes(&mut window);


        window.render();
        plane.unlink();
        wireframe_plane.unlink();

        for event in window.events().iter() {
            match event.value {
                WindowEvent::Close => {
                    break 'main
                }
                WindowEvent::Key(Key::Space, Action::Press, _) => {
                    paused = !paused;
                }
                WindowEvent::Key(Key::Left, Action::Press, _) => {
                    i -= time_steps_per_iteration
                }
                WindowEvent::Key(Key::Right, Action::Press, _) => {
                    i += time_steps_per_iteration
                }
                WindowEvent::Key(Key::R, Action::Press, _) => {
                    draw_relative_values = !draw_relative_values;
                }
                WindowEvent::Key(Key::C, Action::Press, _) => {
                    enable_capping = !enable_capping;
                }
                _ => {}
            }
        }
    }
}

fn draw_grid(window: &mut Window)
{
    for pos in 0..=10
    {
        let pos = pos as f32;
        if pos != 0.
        {
            window.draw_line(
                &Point3::new(pos / 10., 0., -0.1),
                &Point3::new(pos / 10., 0., 1.1),
                &Point3::new(0.5, 0.5, 0.5)
            );
            window.draw_line(
                &Point3::new(-0.1, 0., pos / 10.),
                &Point3::new(1.1, 0., pos / 10.),
                &Point3::new(0.5, 0.5, 0.5)
            );
        }
    }
}

fn draw_axes(window: &mut Window) {
    window.draw_line(
        &Point3::new(0., 0., 0.),
        &Point3::new(1., 0., 0.),
        &Point3::new(1., 0., 0.)
    );
    window.draw_line(
        &Point3::new(0., 0., 0.),
        &Point3::new(0., 1., 0.),
        &Point3::new(0., 1., 0.)
    );
    window.draw_line(
        &Point3::new(0., 0., 0.),
        &Point3::new(0., 0., 1.),
        &Point3::new(0., 0., 1.)
    );
}


fn draw_state_info(
    window: &mut Window,
    font: &Rc<Font>,
    time: usize,
    relative_values: bool,
    enable_capping: bool,
) {
    window.draw_text(
        &format!("time: {}", time),
        &Point2::new(0.0, 120.0),
        60.0,
        font,
        &Point3::new(1.0, 1.0, 0.0),
    );
    window.draw_text(
        &format!("relative_mode: {}", relative_values),
        &Point2::new(0.0, 250.0),
        60.0,
        font,
        &Point3::new(1.0, 1.0, 0.0),
    );
    window.draw_text(
        &format!("capping: {}", enable_capping),
        &Point2::new(0.0, 320.0),
        60.0,
        font,
        &Point3::new(1.0, 1.0, 0.0),
    );
}

// A material that draws the y coordinate in different colors
pub struct YColorMaterial {
    shader: Effect,
    position: ShaderAttribute<Point3<f32>>,
    view: ShaderUniform<Matrix4<f32>>,
    proj: ShaderUniform<Matrix4<f32>>,
    transform: ShaderUniform<Matrix4<f32>>,
    scale: ShaderUniform<Matrix3<f32>>,
}

impl YColorMaterial {
    pub fn new() -> YColorMaterial {
        let mut shader = Effect::new_from_str(NORMAL_VERTEX_SRC, NORMAL_FRAGMENT_SRC);

        shader.use_program();

        YColorMaterial {
            position: shader.get_attrib("position").unwrap(),
            transform: shader.get_uniform("transform").unwrap(),
            scale: shader.get_uniform("scale").unwrap(),
            view: shader.get_uniform("view").unwrap(),
            proj: shader.get_uniform("proj").unwrap(),
            shader: shader,
        }
    }
}

impl Material for YColorMaterial {
    fn render(
        &mut self,
        pass: usize,
        transform: &Isometry3<f32>,
        scale: &Vector3<f32>,
        camera: &mut dyn Camera,
        _: &Light,
        data: &ObjectData,
        mesh: &mut Mesh,
    ) {
        self.shader.use_program();
        self.position.enable();

        /*
         *
         * Setup camera and light.
         *
         */
        camera.upload(pass, &mut self.proj, &mut self.view);

        // enable/disable culling.
        let ctxt = Context::get();
        if data.backface_culling_enabled() {
            ctxt.enable(Context::CULL_FACE);
        } else {
            ctxt.disable(Context::CULL_FACE);
        }

        /*
         *
         * Setup object-related stuffs.
         *
         */
        let formated_transform = transform.to_homogeneous();
        let formated_scale = Matrix3::from_diagonal(&Vector3::new(scale.x, scale.y, scale.z));

        self.transform.upload(&formated_transform);
        self.scale.upload(&formated_scale);

        mesh.bind_coords(&mut self.position);
        mesh.bind_faces();

        Context::get().draw_elements(
            Context::TRIANGLES,
            mesh.num_pts() as i32,
            Context::UNSIGNED_SHORT,
            0,
        );

        mesh.unbind();

        self.position.disable();
    }
}

// A material that draws the y coordinate in different colors
pub struct BlackLineMaterial {
    shader: Effect,
    position: ShaderAttribute<Point3<f32>>,
    view: ShaderUniform<Matrix4<f32>>,
    proj: ShaderUniform<Matrix4<f32>>,
    transform: ShaderUniform<Matrix4<f32>>,
    scale: ShaderUniform<Matrix3<f32>>,
}

impl BlackLineMaterial {
    pub fn new() -> BlackLineMaterial {
        let mut shader = Effect::new_from_str(NORMAL_VERTEX_SRC, WIREFRAME_FRAGMENT_SRC);

        shader.use_program();

        BlackLineMaterial {
            position: shader.get_attrib("position").unwrap(),
            transform: shader.get_uniform("transform").unwrap(),
            scale: shader.get_uniform("scale").unwrap(),
            view: shader.get_uniform("view").unwrap(),
            proj: shader.get_uniform("proj").unwrap(),
            shader: shader,
        }
    }
}

impl Material for BlackLineMaterial {
    fn render(
        &mut self,
        pass: usize,
        transform: &Isometry3<f32>,
        scale: &Vector3<f32>,
        camera: &mut dyn Camera,
        _: &Light,
        _data: &ObjectData,
        mesh: &mut Mesh,
    ) {
        self.shader.use_program();
        self.position.enable();

        /*
         *
         * Setup camera and light.
         *
         */
        camera.upload(pass, &mut self.proj, &mut self.view);

        // enable/disable culling.
        let ctxt = Context::get();

        ctxt.polygon_mode(Context::FRONT_AND_BACK, Context::LINE);

        /*
         *
         * Setup object-related stuffs.
         *
         */
        let formated_transform = transform.to_homogeneous();
        let formated_scale = Matrix3::from_diagonal(&Vector3::new(scale.x, scale.y, scale.z));

        self.transform.upload(&formated_transform);
        self.scale.upload(&formated_scale);

        mesh.bind_coords(&mut self.position);
        mesh.bind_faces();

        Context::get().draw_elements(
            Context::TRIANGLES,
            mesh.num_pts() as i32,
            Context::UNSIGNED_SHORT,
            0,
        );

        mesh.unbind();

        self.position.disable();
    }
}

static NORMAL_VERTEX_SRC: &'static str = "#version 100
attribute vec3 position;
uniform mat4 view;
uniform mat4 proj;
uniform mat4 transform;
uniform mat3 scale;
varying vec3 ls_position;
void main() {
    ls_position = position;
    gl_Position = proj * view * transform * mat4(scale) * vec4(position, 1.0);
}
";

static NORMAL_FRAGMENT_SRC: &'static str = "#version 100
#ifdef GL_FRAGMENT_PRECISION_HIGH
   precision highp float;
#else
   precision mediump float;
#endif
varying vec3 ls_position;

// https://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl
// All components are in the range [0…1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    float saturation = 0.8;
    if (ls_position.y < 0.) {
        saturation = 0.1;
    }
    vec3 hsv = vec3(ls_position.y, saturation, 0.8);

    gl_FragColor = vec4(hsv2rgb(hsv), 1.);
}
";



static WIREFRAME_FRAGMENT_SRC: &'static str = "#version 100
#ifdef GL_FRAGMENT_PRECISION_HIGH
   precision highp float;
#else
   precision mediump float;
#endif
varying vec3 ls_position;

void main() {
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.);
}
";
