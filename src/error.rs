extern crate core;
extern crate failure;

use core::fmt::{self, Display};
use failure::{Backtrace, Context, Fail};

#[derive(Debug)]
pub struct Error {
    inner: Context<String>,
}

impl Fail for Error {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.inner, f)
    }
}

// Allows writing `Error::from("oops"))?`
impl From<&'static str> for Error {
    fn from(msg: &'static str) -> Error {
        Error {
            inner: Context::new(msg.into()),
        }
    }
}
impl From<String> for Error {
    fn from(msg: String) -> Error {
        Error {
            inner: Context::new(msg.into()),
        }
    }
}

// Allows adding more context via a String
impl From<Context<String>> for Error {
    fn from(inner: Context<String>) -> Error {
        Error { inner }
    }
}

// Allows adding more context via a &str
impl From<Context<&'static str>> for Error {
    fn from(inner: Context<&'static str>) -> Error {
        Error {
            inner: inner.map(|s| s.to_string()),
        }
    }
}

