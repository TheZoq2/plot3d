import numpy as np


X = np.arange(-5, 5, 0.25)
Y = np.arange(-10, 10, 0.3)

with open("test_data/x_points", "w") as f:
    for x in X:
        f.write(f"{x}\n")
with open("test_data/y_points", "w") as f:
    for y in Y:
        f.write(f"{y}\n")

with open("test_data/z_points", "w") as f:
    for t in range(-314, 314):
        time = t / 10.;

        X_, Y_ = np.meshgrid(X, Y)
        R = np.sqrt(X_**2 + Y_**2)
        Z = np.sin(R + time)
        for x in range(len(X)):
            for y in range(len(Y)):
                f.write(f"{Z[y][x]}")
                f.write(",")
        f.write("\n")




